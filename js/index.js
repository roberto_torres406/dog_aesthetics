$(function(){
    $('[data-toggle="tooltip"]').tooltip();    
    $('[data-toggle="popover"]').popover({
        container: 'body'
    });
    $('.carousel').carousel({
        interval: 3500
    });
}); 